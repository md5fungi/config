local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"

LBRACE="%{$fg_bold[yellow]%}{"
RBRACE="%{$fg_bold[yellow]%}}"

PROMPT='${ret_status} $USER${LBRACE}%{$fg[cyan]%}%c${RBRACE}%{$reset_color%} $(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$fg_bold[blue]%})%{$reset_color%} "
