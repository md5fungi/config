#!/usr/factorysystems/bin/perl
# ysrivastava

use strict;
use warnings;


#############################################################
# GLOBALS 
#############################################################



#############################################################
# MAIN 
#############################################################

parse_args();











#############################################################
# SUBROUTINES 
#############################################################

sub parse_args
{
    foreach my $arg (@ARGV)
    {
        if ($arg =~ /^-*h(elp)?$/)
        {
            print_usage();
            exit 0;
        }
        else 
        {
            die "Error: '$arg' is not a valid argument.\n\n";
        }
    }
}

sub print_usage
{
    print "Usage: script.pl [OPTION]\n\n";
    print "Description goes here.\n\n";
    print "OPTIONS\n\n";
    print "-help                           Print out this help information\n\n";
}



__END__
