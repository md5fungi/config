autocmd BufRead,BufNewFile   *.py            set filetype=python
autocmd BufNewFile           *.py            0r ~/.vim/skeleton/python.vim
