autocmd BufRead,BufNewFile   *.php     set filetype=php
autocmd BufNewFile           *.php     0r ~/.vim/skeleton/php.vim
