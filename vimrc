"Generic Vim Settings
filetype plugin on
set nocompatible
set number
syntax on
highlight comment ctermfg=lightblue
set nowrap
set nowb
set ts=4
set expandtab
set shiftwidth=4

"Diff Settings
if &diff
colorscheme slate   
else
colorscheme default                          
endif
set diffopt+=iwhite


"Key Mappings
map <C-y> "+y
map <C-p> "+p
map <F2> :syntax off<CR>
map <F3> :syntax on<CR>
map <F4> :set wrap<CR>
map <F5> :set nowrap<CR>
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
